![](/icons/PackIcon/bitmap-png/SodiumPack.png) ![](/icons/PackIcon/bitmap-png/ChlorinePack.png) ![](/icons/PackIcon/bitmap-png/HalogenPack.png) ![](/icons/PackIcon/bitmap-png/MagnesiumPack.png) ![](/icons/PackIcon/bitmap-png/RubidiumPack.png) ![](/icons/PackIcon/bitmap-png/Sodium-ExtraPack.png) ![](/icons/PackIcon/bitmap-png/BSVSBPack.png) ![](/icons/PackIcon/bitmap-png/SodiumAllPack.png) ![](/icons/PackIcon/bitmap-png/Magnesium&BMVSBPack.png)

# Sodium Translation Pack

EN / [ZH-CN](README.md)

[TOC]

## What's this ?

Translation resource packs for Sodium and its affiliates and Forge branches .

Localization is supported in [CaffeineMC / sodium-fabric](https://github.com/CaffeineMC/sodium-fabric) Pull [#100](https://github.com/CaffeineMC/sodium-fabric/pull/100) [#517](https://github.com/CaffeineMC/sodium-fabric/pull/517) [#717](https://github.com/CaffeineMC/sodium-fabric/pull/717) and integrated into [CaffeineMC / sodium-fabric](https://github.com/CaffeineMC/sodium-fabric) / [1.17.x / dev](https://github.com/CaffeineMC/sodium-fabric/tree/1.17.x/dev) . However , the translation on ~~[OneSky](https://jellysquid.oneskyapp.com/collaboration/project?id=366422)~~ [Crowdin](https://crowdin.com/translate/sodium-fabric) is not moved into the Sodium .

### Mod and game version support :

|       ResourcePack Name       |   Mod Support   | Game Version Support | ModLoader |
| :---------------------------: | :-------------: | :------------------: | :-------: |
|     SodiumTranslationPack     |     Sodium      |    ~~1.16.x &~~1.17.x & 1.18.x    |  Fabric   |
| HalogenTranslationPack | Halogen |        1.16.5        |   Forge   |
|  Sodium-ExtraTranslationPack  |  Sodium-Extra   |       1.17.x & 1.18.x       |  Fabric   |
| BSVSB&Sodium&SodiumExtraTranslation | Sodium & Sodium-Extra & Better Sodium Video Settings Button |    1.17.x & 1.18.x    |  Fabric   |
|        MagnesiumTranslationPack         |                          Magnesium                          | ~~1.16.5~~1.18.x |   Forge    |
|         ChlorineTranslationPack         |                          Chlorine                           |      1.16.5      |   Forge    |
|     Magnesium&BMVSBTranslationPack      |     Magnesium & Better Magnesium Video Settings Button      | ~~1.16.5~~1.18.x |   Forge    |
| RubidiumTranslationPack | Rubidium | 1.18.x | Forge |




### Thanks :

[Sodium](https://github.com/jellysquid3/sodium-fabric) author: **JellySquid**

[Chlorine](https://github.com/HalogenMods/Chlorine) author: **hanetzer**

[Halogen](https://github.com/spoorn/sodium-forge) author: **sporn**

[Magnesium](https://github.com/Someone-Else-Was-Taken/Magnesium) author: **someoneelsewastaken**

[Rubidium](https://github.com/Asek3/Rubidium) author: **Asek3**

[Sodium-Extra](https://github.com/FlashyReese/sodium-extra-fabric) author: **Flashy Reese**

[Better Sodium Video Settings Button](https://github.com/LimeShulkerBox/better-sodium-video-settings) & [Better Magnesium Video Settings Button](https://github.com/LimeShulkerBox/better-magnesium-video-settings-button) author: **LimeShulkerBox**

**MojangStudio**

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License(CC-BY-SA)</a>.



# Sodium Translation Project

<script src="https://gist.github.com/amnotbananaama/6cfd6ce27e3258d50db6a4fd9291aa94.js"></script>
