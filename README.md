![](/icons/PackIcon/bitmap-png/SodiumPack.png) ![](/icons/PackIcon/bitmap-png/ChlorinePack.png) ![](/icons/PackIcon/bitmap-png/HalogenPack.png) ![](/icons/PackIcon/bitmap-png/MagnesiumPack.png) ![](/icons/PackIcon/bitmap-png/RubidiumPack.png) ![](/icons/PackIcon/bitmap-png/Sodium-ExtraPack.png) ![](/icons/PackIcon/bitmap-png/BSVSBPack.png) ![](/icons/PackIcon/bitmap-png/SodiumAllPack.png) ![](/icons/PackIcon/bitmap-png/Magnesium&BMVSBPack.png)

# Sodium Translation Pack

[EN](README-EN.md) / ZH-CN



## 这是什么？

这是Sodium及其附属和Forge分支的翻译资源包

在 [CaffeineMC/sodium-fabric](https://github.com/CaffeineMC/sodium-fabric) Pull Request [#100](https://github.com/CaffeineMC/sodium-fabric/pull/100) 、 [#517](https://github.com/CaffeineMC/sodium-fabric/pull/517) 、[#717](https://github.com/CaffeineMC/sodium-fabric/pull/717) 
对本地化进行了支持并被整合到 [CaffeineMC/sodium-fabric](https://github.com/CaffeineMC/sodium-fabric) / [1.17.x/dev](https://github.com/CaffeineMC/sodium-fabric/tree/1.17.x/dev) 分支,
但由于作者未将 ~~[OneSky](https://jellysquid.oneskyapp.com/collaboration/project?id=366422)~~ [Crowdin](https://crowdin.com/translate/sodium-fabric) 上的翻译移动到模组内,此材质包提供了简体中文翻译
(有可能停止更新Sodium翻译，详见 [Sodium Translation Project - 简体中文说明](https://gist.github.com/TexBlock/1f5628574b1d6cd7df7243c8bcc552c6) )
### 模组及游戏版本支持
|                  材质包名称                  |                            支持模组                             |    游戏版本支持     | 模组加载器 |
|:---------------------------------------:|:-----------------------------------------------------------:|:-------------:| :----------: |
|          SodiumTranslationPack          |                           Sodium                            | 1.17.x&1.18.x |   Fabric   |
|         HalogenTranslationPack          |                           Halogen                           | 1.16.x&1.17.x |   Forge   |
|       Sodium-ExtraTranslationPack       |                        Sodium-Extra                         | 1.17.x&1.18.x |   Fabric   |
| BSVSB&Sodium&SodiumExtraTranslationPack | Sodium & Sodium-Extra & Better Sodium Video Settings Button | 1.17.x&1.18.x |   Fabric   |
|          Mg/RbTranslationPack           |                          Magnesium/Rubidium                          |    1.18.x     |   Forge   |
|         ChlorineTranslationPack         |                          Chlorine                           |    1.16.x     |   Forge   |
|       Mg/Rb&BMVSBTranslationPack        | Magnesium/Rubidium & Better Magnesium Video Settings Button |    1.18.x     |   Forge   |

> Sodium及其附属 1.16.x 翻译: [【Sodium】钠模组汉化 游戏帧数优化三件套 - 哔哩哔哩专栏](https://www.bilibili.com/read/cv6832123/)
>
> Magnesium及其附属 1.16.x 翻译: [Molarczsq/Magnesium-should-be-in-Chinese](https://github.com/Molarczsq/Magnesium-should-be-in-Chinese/releases)

### 感谢

[Sodium](https://github.com/jellysquid3/sodium-fabric) 作者: **JellySquid**

[Chlorine](https://github.com/HalogenMods/Chlorine) 作者: **hanetzer**

[Halogen](https://github.com/spoorn/sodium-forge) 作者: **spoorn**

[Magnesium](https://github.com/Someone-Else-Was-Taken/Magnesium) 作者: **someoneelsewastaken**

[Rubidium](https://github.com/Asek3/Rubidium) 作者: **Asek3**

[Sodium-Extra](https://github.com/FlashyReese/sodium-extra-fabric) 作者: **FlashyReese**

[Better Sodium Video Settings Button](https://github.com/LimeShulkerBox/better-sodium-video-settings) & [Better Magnesium Video Settings Button](https://github.com/LimeShulkerBox/better-magnesium-video-settings-button) 作者: **LimeShulkerBox**

**MojangStudio**

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="知识共享许可协议" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>本作品（简体中文语言文件）采用<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">知识共享署名-相同方式共享 4.0 国际许可协议(CC-BY-SA)</a>进行许可。

